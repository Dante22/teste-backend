#!/bin/sh
while ! nc -z database 5432; 
do 
    echo 'Banco de dados ainda não está pronto, esperando container database..';
    sleep 2; 
done;
echo 'Container database pronto para receber conexões, iniciando aplicação';

npm run start
npm run typeorm migration:run
npm run swagger