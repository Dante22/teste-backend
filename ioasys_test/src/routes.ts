import * as express from 'express';
import UserController from './controller/UserController';
import MovieController from './controller/MovieController';
import createUserValidation from './validationSchemas/CreateUserValidation';
import jwt = require('jsonwebtoken');
import passport = require('passport');
import validateUniqueEmail from './validationSchemas/ValidateUniqueEmail';
import rateMovieValidation from './validationSchemas/RateMovieValidation';
import createMovieValidation from './validationSchemas/CreateMovieValidation';
const routes = express.Router();
require('./auth/auth')(passport);

const secretKey = process.env.JWT_SIGNATURE_KEY;

routes.post(
  '/users/create-admin',
  createUserValidation,
  validateUniqueEmail,
  UserController.createAdmin
);

routes.post(
  '/users/create-user',
  createUserValidation,
  validateUniqueEmail,
  UserController.createUser
);

routes.put(
  '/users/update/:id',
  passport.authenticate('jwt', { session: false }),
  UserController.updateUser
);

routes.delete(
  '/users/delete/:id',
  passport.authenticate('jwt', { session: false }),
  UserController.deactivateUser
);

routes.post('/users/login', (req, res, next) => {
  /* 
  #swagger.tags = ['Usuarios']
  #swagger.description = 'Rota para realizar login de um usuario'
  */

  passport.authenticate('local', { session: false }, (err, user, info) => {
    if (err) {
      return res.status(500).json({ err });
    }

    if (!user) {
      const { message } = info;
      return res.status(401).json({ message });
    }

    if (!user.active) {
      return res.status(401).json({ msg: 'Esta conta esta desativada!' });
    }

    const { id } = user;
    const token = jwt.sign({ id }, secretKey, { expiresIn: '1h' });

    res
      .cookie('jwt', token, {
        httpOnly: false,
        secure: false,
      })
      .status(200)
      .send({ msg: 'Logado com sucesso!' });
  })(req, res, next);
});

routes.post(
  '/movies/create',
  passport.authenticate('jwt-admin', { session: false }),
  createMovieValidation,
  MovieController.createMovie
);

routes.get(
  '/movies/get',
  passport.authenticate('jwt', { session: false }),
  MovieController.getMovieList
);

routes.get(
  '/movies/get/:id',
  passport.authenticate('jwt', { session: false }),
  MovieController.getMovieDetails
);

routes.post(
  '/movies/rate/:movieId',
  passport.authenticate('jwt-user', { session: false }),
  rateMovieValidation,
  MovieController.rateMovie
);

export default routes;
