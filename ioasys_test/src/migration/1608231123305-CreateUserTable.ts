import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateUserTable1608231123305 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Criando tipo enum de sexo para usuario');

    await queryRunner.query(
      "CREATE TYPE sex_type as ENUM('Male', 'Female', 'Other', 'N/I');"
    );

    console.log('Criando tabela de usuarios');

    await queryRunner.createTable(
      new Table({
        name: 'users',
        columns: [
          { name: 'id', type: 'int', isPrimary: true, isGenerated: true },
          { name: 'name', type: 'varchar', length: '255' },
          { name: 'email', type: 'varchar', length: '100' },
          { name: 'pwd', type: 'varchar', length: '255' },
          {
            name: 'sex',
            type: 'sex_type',
            isNullable: true,
            default: `'N/I'`,
          },
          { name: 'active', type: 'boolean' },
          { name: 'salt', type: 'varchar' },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    console.log('Removendo tipo enum de sexo para usuario');

    await queryRunner.query('DROP TYPE sex_type;');

    console.log('Removedo tabela de usuarios');
    await queryRunner.dropTable('users');
  }
}
