import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateMovieTable1608270070396 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Criando tabela de filmes');

    await queryRunner.createTable(
      new Table({
        name: 'movies',
        columns: [
          { name: 'id', type: 'int', isGenerated: true, isPrimary: true },
          { name: 'name', type: 'varchar' },
          { name: 'genre', type: 'varchar' },
          { name: 'director', type: 'varchar' },
          { name: 'cast', type: 'varchar' },
        ],
      })
    );

    await queryRunner.createTable(
      new Table({
        name: 'movies_rate',
        columns: [
          { name: 'movieId', type: 'int' },
          { name: 'userId', type: 'int' },
          { name: 'rate', type: 'int' },
        ],
      })
    );

    await queryRunner.createPrimaryKey('movies_rate', ['movieId', 'userId']);

    await queryRunner.createForeignKey(
      'movies_rate',
      new TableForeignKey({
        columnNames: ['userId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
        onDelete: 'CASCADE',
      })
    );

    await queryRunner.createForeignKey(
      'movies_rate',
      new TableForeignKey({
        columnNames: ['movieId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'movies',
        onDelete: 'CASCADE',
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('movies_rate');
    await queryRunner.dropTable('movies');
  }
}
