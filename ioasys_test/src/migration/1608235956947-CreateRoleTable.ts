import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateRoleTable1608235956947 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('Criando tabela de papeis');

    await queryRunner.createTable(
      new Table({
        name: 'roles',
        columns: [
          { name: 'id', type: 'int', isGenerated: true, isPrimary: true },
          { name: 'name', type: 'varchar', length: '50' },
          { name: 'description', type: 'text' },
        ],
      })
    );

    console.log('Cadastrando papel de administrador');

    await queryRunner.query(
      "INSERT INTO roles values (default, 'admin', 'Papel de administrador, capaz de cadastrar novos filmes');"
    );

    console.log('Cadastrando papel de usuario');

    await queryRunner.query(
      "INSERT INTO roles values (default, 'user', 'Papel de usuario, capaz de avaliar filmes ja cadastrados');"
    );

    await queryRunner.createTable(
      new Table({
        name: 'users_roles',
        columns: [
          { name: 'usersId', type: 'int' },
          { name: 'rolesId', type: 'int' },
        ],
      })
    );

    await queryRunner.createPrimaryKey('users_roles', ['usersId', 'rolesId']);

    await queryRunner.createForeignKey(
      'users_roles',
      new TableForeignKey({
        columnNames: ['usersId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
        onDelete: 'CASCADE',
      })
    );

    await queryRunner.createForeignKey(
      'users_roles',
      new TableForeignKey({
        columnNames: ['rolesId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'roles',
        onDelete: 'CASCADE',
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    console.log('Removendo tabela de papeis');

    await queryRunner.dropTable('roles');
  }
}
