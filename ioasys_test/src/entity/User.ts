import { Exclude } from 'class-transformer/decorators';
import { IsEnum, IsOptional } from 'class-validator';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Sex } from '../utils/sex.enum';
import { Role } from './Role';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column({ select: false })
  @Exclude({ toPlainOnly: true })
  pwd: string;

  @Column({ select: false })
  salt: string;

  @Column({ nullable: true, type: 'enum', enum: Sex, default: Sex.UNTOLD })
  @IsEnum(Sex)
  @IsOptional()
  sex?: Sex = Sex.UNTOLD;

  @ManyToMany(() => Role, { eager: true })
  @JoinTable({ name: 'users_roles' })
  roles: Role[];

  @Column()
  @IsOptional()
  active?: boolean = true;
}
