import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { MovieRate } from './MovieRate';

@Entity({ name: 'movies' })
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  genre: string;

  @Column()
  director: string;

  @Column('varchar', { array: true })
  cast: string[];

  @OneToMany(() => MovieRate, (movieRate) => movieRate.movie, { eager: true })
  rates: MovieRate[];

  averageRate: number = 0;
}
