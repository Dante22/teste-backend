import {
  Column,
  Entity,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Movie } from './Movie';

@Entity({ name: 'movies_rate' })
export class MovieRate {
  @PrimaryColumn({ name: 'movieId' })
  movieId: number;

  @PrimaryColumn({ name: 'userId' })
  userId: number;

  @Column()
  rate: number;

  @ManyToOne(() => Movie, (movie) => movie.rates)
  movie: Movie;
}
