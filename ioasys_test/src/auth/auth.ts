import passport = require('passport');
import { getRepository } from 'typeorm';
import { User } from '../entity/User';
import { Roles } from '../utils/role.enum';

const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

var crypto = require('crypto');

const secretKey = process.env.JWT_SIGNATURE_KEY;

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secretKey,
};

module.exports = function (passport) {
  passport.use(
    'local',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'pwd',
      },
      async function (email, password, done) {
        const user: User = await getRepository(User).findOne({
          select: ['id', 'name', 'email', 'sex', 'active', 'salt', 'pwd'],
          where: { email: email },
        });

        if (!user) {
          return done(null, false, { message: 'Usuario nao encontrado' });
        }

        const hash = crypto
          .pbkdf2Sync(password, user.salt, 1000, 64, `sha512`)
          .toString(`hex`);

        if (hash != user.pwd) {
          return done(null, false, { message: 'Senha incorreta' });
        }

        return done(null, user);
      }
    )
  );

  passport.use(
    'jwt',
    new JwtStrategy(opts, async (payload, done) => {
      const user: User = await getRepository(User).findOne({ id: payload.id });

      if (!user) {
        return done(null, false);
      }

      return done(null, { user: user });
    })
  );

  passport.use(
    'jwt-admin',
    new JwtStrategy(opts, async (payload, done) => {
      const user: User = await getRepository(User).findOne({ id: payload.id });

      if (!user) {
        return done(null, false);
      }

      if (!user.roles.map((role) => role.name).includes(Roles.Admin)) {
        return done(null, false, {
          message: 'Apenas administradores podem realizar esta acao',
        });
      }

      return done(null, { user: user });
    })
  );

  passport.use(
    'jwt-user',
    new JwtStrategy(opts, async (payload, done) => {
      const user: User = await getRepository(User).findOne({ id: payload.id });

      if (!user) {
        return done(null, false);
      }

      if (!user.roles.map((role) => role.name).includes(Roles.User)) {
        return done(null, false, {
          message: 'Apenas usuarios comuns podem realizar esta acao',
        });
      }

      return done(null, { user: user });
    })
  );
};
