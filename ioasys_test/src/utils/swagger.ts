import { string } from 'joi';

const swaggerAutogen = require('swagger-autogen')();

const outputFile = './apis_doc.json';
const endpointsFiles = ['./src/routes.ts'];

const doc = {
  info: {
    version: '1.0.0',
    title: 'IOasys teste',
    description: 'Conjunto de APIs para o teste da IOasys',
  },
  host: 'localhost:3000',
  basePath: '/',
  schemes: ['http', 'https'],
  consumes: ['application/json'],
  produces: ['application/json'],
  definitions: {
    User: {
      name: 'Jhon Doe',
      email: 'jhondoe@gmail.com',
      pwd: '123456',
      sex: {
        type: 'array',
        items: ['Male', 'Female'],
      },
    },
    Movie: {
      name: 'Matrix',
      director: 'Lilly Wachowski',
      genre: 'Ficção científica',
      cast: [
        'Keanu Reeves',
        'Laurence Fishburne',
        'Hugo Weaving',
        'Carrie-Anne Moss',
      ],
    },
    Rate: {
      rate: 3,
    },
  },
};

swaggerAutogen(outputFile, endpointsFiles, doc);
