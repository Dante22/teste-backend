import * as express from 'express';
import routes from './routes';
import { createConnection } from 'typeorm';
import passport = require('passport');
const { validate, ValidationError, Joi } = require('express-validation');
const swaggerUi = require('swagger-ui-express');
const swaggerFile = require('../apis_doc.json');
require('./auth/auth')(passport);

const app = express();

createConnection().then((connection) => {
  app.use(passport.initialize());
  app.use(express.json());
  app.use(routes);
  app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));
});

app.listen(3000, () => {
  console.log('Aplicacao iniciada na porta 3000');
});
