import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import { User } from '../entity/User';
import { Role } from '../entity/Role';
import { classToPlain } from 'class-transformer';
import { Roles } from '../utils/role.enum';

var crypto = require('crypto');

function generateSaltAndHash(pwd: string) {
  const salt = crypto.randomBytes(16).toString('hex');

  const hash = crypto.pbkdf2Sync(pwd, salt, 1000, 64, `sha512`).toString(`hex`);

  return { hash: hash, salt: salt };
}

const createAdmin = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Usuarios']
  #swagger.description = 'Rota para cadastrar um usuario administrador'
  #swagger.parameters['obj'] = { 
    in: 'body',
    description: "User data.",
    schema: { $ref: "#/definitions/User" }
  } */

  let userData: User = req.body;

  console.log(
    `Iniciando rota para cadastrar administrador.\nDados recebidos: ${JSON.stringify(
      classToPlain(userData)
    )}`
  );

  const { hash, salt } = generateSaltAndHash(userData.pwd);
  userData.pwd = hash;
  userData.salt = salt;

  const adminRole: Role = await getRepository(Role).findOne({
    name: Roles.Admin,
  });

  userData.roles = [adminRole];

  const user = getRepository(User).create(userData);
  const result = await getRepository(User).save(user);
  return res.json({ msg: 'Administrador criado com sucesso', id: user.id });
};

const createUser = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Usuarios']
  #swagger.description = 'Rota para cadastrar um usuario comum'
  #swagger.parameters['obj'] = { 
    in: 'body',
    description: "User data.",
    schema: { $ref: "#/definitions/User" }
  } */

  let userData: User = req.body;

  console.log(
    `Iniciando rota para cadastrar usuario.\nDados recebidos: ${JSON.stringify(
      classToPlain(userData)
    )}`
  );

  const { hash, salt } = generateSaltAndHash(userData.pwd);
  userData.pwd = hash;
  userData.salt = salt;
  const userRole: Role = await getRepository(Role).findOne({
    name: Roles.User,
  });

  userData.roles = [userRole];

  const user = getRepository(User).create(userData);
  const result = await getRepository(User).save(user);
  return res.json({ msg: 'Usuario criado com sucesso', id: user.id });
};

const updateUser = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Usuarios']
  #swagger.description = 'Rota para alterar dados de um usuario, tanto comum quanto administrador, utiliza autenticacao Bearer'
  #swagger.parameters['obj'] = { 
    in: 'body',
    description: "User data.",
    schema: { $ref: "#/definitions/User" }
  } */

  console.log('Iniciando rota para alteracao de dados de usuario');

  const updateData: User = req.body;

  const userRepository = getRepository(User);

  let user: User = await userRepository.findOne(req.params.id);

  userRepository.merge(user, updateData);

  await userRepository.save(user);

  return res.json({ msg: 'Dados de usuario atualizados com sucesso!' });
};

const deactivateUser = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Usuarios']
  #swagger.description = 'Rota para desativar conta de um usuario,  utiliza autenticacao Bearer'
  */

  console.log('Iniciando rota para desativar conta de usuario');

  const userRepository = getRepository(User);

  let user: User = await userRepository.findOne(req.params.id);

  await userRepository.update(user.id, { active: false });

  return res.json({ msg: 'Conta desativada com sucesso!' });
};

export default { createAdmin, createUser, updateUser, deactivateUser };
