import { getRepository, SelectQueryBuilder } from 'typeorm';
import { query, Request, Response } from 'express';
import { Movie } from '../entity/Movie';
import { MovieRate } from '../entity/MovieRate';
import { use } from 'passport';

const createMovie = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Filmes']
  #swagger.description = 'Rota para cadastrar um filme, so pode ser executada por um usuario administrador, utiliza autenticacao Bearer'
  #swagger.parameters['obj'] = { 
    in: 'body',
    description: "Dados do filme.",
    schema: { $ref: "#/definitions/Movie" }
  } */

  console.log('Iniciando rota para cadastrar filme');

  const movieData: Movie = req.body;

  const movie = getRepository(Movie).create(movieData);

  const result = await getRepository(Movie).save(movie);

  return res.json({ msg: 'Filme cadastrado com sucesso!', id: movie.id });
};

const getMovieList = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Filmes']
  #swagger.description = 'Rota para obter uma lista com id e nome de filmes, so pode ser executada por um usuario logado(administrador ou comum), utiliza autenticacao Bearer'
  */

  console.log('Iniciando rota para obter lista de filmes');

  const name = req.query.name;
  const director = req.query.director;
  const genre = req.query.genre;
  const cast = req.query.cast;

  let query: SelectQueryBuilder<Movie> = getRepository(Movie)
    .createQueryBuilder('movies')
    .select('id')
    .addSelect('name');

  query = name ? query.andWhere('movies.name = :name', { name: name }) : query;

  query = director
    ? query.andWhere('movies.director = :director', { director: director })
    : query;

  query = genre
    ? query.andWhere('movies.genre = :genre', { genre: genre })
    : query;

  query = cast
    ? query.andWhere('movies.cast IN (:...cast)]', {
        cast: cast,
      })
    : query;

  const movieList = await query.orderBy('movies.name', 'ASC').getRawMany();

  console.log('Lista ' + JSON.stringify(movieList));

  return res.status(200).json(movieList);
};

const getMovieDetails = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Filmes']
  #swagger.description = 'Rota para obter detalhes de um filme, so pode ser executada por um usuario logado(administrador ou comum), utiliza autenticacao Bearer'
  */

  console.log('Iniciando rota para obter detalhes de filme');

  const id = req.params.id;

  let movie = await getRepository(Movie).findOne(id);

  if (!movie) {
    return res
      .status(404)
      .json({ msg: `Nao foi encontrado filme com id ${id}` });
  }

  movie.averageRate =
    movie.rates.reduce(function (acc, r: MovieRate) {
      return acc + r.rate;
    }, 0) / movie.rates.length;

  return res.status(200).json(movie);
};

const rateMovie = async (req: Request, res: Response) => {
  /* 
  #swagger.tags = ['Filmes']
  #swagger.description = 'Rota para avaliar um filme, so pode ser executada por um usuario comum, utiliza autenticacao Bearer'
  #swagger.parameters['obj'] = { 
    in: 'body',
    description: "Nota do filme",
    schema: { $ref: "#/definitions/Rate" }
  } */

  const user = req.user['user'];
  const movieId = req.params.movieId;

  const rate = req.body.rate;

  let movie = await getRepository(Movie).findOne(movieId);

  if (!movie) {
    return res
      .status(404)
      .json({ msg: `Nao foi encontrado filme com id ${movieId}` });
  }

  if (
    await getRepository(MovieRate).findOne({
      where: { movieId: movieId, userId: user.id },
    })
  ) {
    return res.status(403).json({ msg: 'Voce ja avaliou esse filme' });
  }

  let movieRate: MovieRate = new MovieRate();
  movieRate.userId = user.id;
  movieRate.movieId = movie.id;
  movieRate.movie = movie;
  movieRate.rate = rate;

  movie.rates.push(movieRate);

  await getRepository(MovieRate).save(movieRate);

  return res.status(200).json('Filme avaliado com sucesso!');
};

export default { createMovie, getMovieList, rateMovie, getMovieDetails };
