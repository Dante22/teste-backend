import { Sex } from '../utils/sex.enum';
import Joi = require('joi');
import validateRequest from './ValidateRequest';

function createUserValidation(req, res, next) {
  const schema = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    pwd: Joi.string().required().min(6).max(20),
    sex: Joi.string().valid(...Object.values(Sex)),
  });

  validateRequest(req, next, schema);
}

export default createUserValidation;
