import { emit } from 'process';
import { getRepository } from 'typeorm';
import { User } from '../entity/User';

async function validateUniqueEmail(req, res, next) {
  const email = req.body.email;

  const user: User = await getRepository(User).findOne({
    email: email,
  });

  if (user) {
    next(`Email ${email} ja esta em uso!`);
  } else {
    next();
  }
}

export default validateUniqueEmail;
