import Joi = require('joi');
import validateRequest from './ValidateRequest';

function createMovieValidation(req, res, next) {
  const schema = Joi.object({
    name: Joi.string().required(),
    genre: Joi.string().required(),
    director: Joi.string().required(),
    cast: Joi.array().required(),
  });

  validateRequest(req, next, schema);
}

export default createMovieValidation;
