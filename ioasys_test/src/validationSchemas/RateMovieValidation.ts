import Joi = require('joi');
import validateRequest from './ValidateRequest';

function rateMovieValidation(req, res, next) {
  const schema = Joi.object({
    rate: Joi.number().integer().required().min(0).max(4),
  });

  validateRequest(req, next, schema);
}

export default rateMovieValidation;
