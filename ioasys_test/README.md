# IOasys teste

## Este projeto foi criado para a participação no processo seletivo da Empresa IOasys. O desafio consistia em desenvolver uma aplicação em NodeJs com um conjunto de APIs que pudessem ser consumidas por uma aplicacao como o IMDB(para avaliacao de filmes) e com persistencia de dados em um banco.

# Estrutura do projeto

O lado do servidor foi feito em NodeJs com express para uma arquitetura REST. Existe também uma camada de banco de dados desenvolvida com PostgreSQL para persistência de dados sobre os usuarios e filmes, bem como as avaliacoes dos usuarios.

---

# Endpoints da API

```
/users/create-admin Recebe os dados de um usuario e cria um registro com permissao de administrador para ele.
```

```
/users/create-user Recebe os dados de um usuario e cria um registro com permissao de usuario para ele.
```

```
/users/login Recebe as credenciais de acesso(e-mail e senha) de um usuario(administrador ou comum) e realiza o login retornando um token jwt.
```

```
/users/update/<userId> Recebe o id do usuario a ter os dados alterados, bem como os dados novos e realiza a atualizacao de tais informacoes em banco
```

```
/users/delete/<userId> Recebe o id do usuario a ter sua conta desativada.
```

```
/movies/create Recebe os dados de um filme e realiza o cadastro em banco.
```

```
/movies/get Busca filmes com base em alguns criterios opcionais de busca como diretor, genero, nome.
```

```
/movies/get/<movieId> Busca um filme pelo id.
```

```
/movies/rate/<movieId> Recebe o id de um filme e sua avaliacao e registra tal informacao em banco.
```

Para ver a documentação completa da API acesse a URL http://localhost:3000/doc apos ter iniciado o servidor.

---

## Dependências utilizadas

Para a aplicação em NodeJs foram usadas, principalmente, as dependências:

- [Express](https://expressjs.com/pt-br/)
  - Uma das versões mais recentes deste framework.
- [Docker](https://www.docker.com/)
  - Ferramenta para deploy da aplicacao.
- [Swagger Autogen](https://www.npmjs.com/package/swagger-autogen)
  - Biblioteca para documentacao da aplicacao.
- [TypeOrm](https://typeorm.io/#/)
  - ORM/ODM para comunicacao com o banco de dados

Para a camada de banco de dados foi utilizada a seguinte dependência:

- [PostgreSQL 12](https://www.postgresql.org/)
  - SGBD relacional em uma de suas versões mais atuais.

---

## Execução da aplicação

O projeto foi configurado para execução com Docker Compose.
Para executar utilizando o Docker Compose, primeiramente, é necessário fazer a instalação do [Docker](https://docs.docker.com/), siga as instruções da documentação para instalar a versão mais apropriada da sua distro. Instalado o Docker, precisamos instalar o [Docker Compose](https://docs.docker.com/compose/install/) para gerenciar os containers da aplicação. Após instalá-lo, basta executar o comando docker-compose up --build (**pode precisar de permissão de super usuário**) na pasta fonte do projeto, onde está localizado o arquivo docker-compose.yml e o processo de download/instalação de dependências se dará de forma automática. O processo de build total da aplicação pode levar em torno de 10 a 15 minutos, dependendo da máquina e conexão com a internet. Quando o Docker Compose completar o processo,a aplicação poderá ser acessada pelo endereço **http://localhost:3000**.
